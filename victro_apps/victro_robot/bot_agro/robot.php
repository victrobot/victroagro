<?php
class BotAgro extends controller_robot {
    public function language($bot_pag = 1, $bot_mod = 1){
        $bot_data = $this->global_robot();
        $bot_model = $this->model("language");
        $bot_data['returned'] = $bot_model->select_language($bot_pag)->get_fetch();
        $bot_data['pags'] = $bot_model->bot_numpags;
        if($bot_mod == 1){
            $this->system_view("language", $bot_data);
        } else {
            $this->view("language", $bot_data);
        }
    }
    public function cadPlanta(){
        $bot_data = $this->global_robot();
        $bot_model = $this->model("language");
				$this->system_view('cadPlanta');				
    }
    public function cadPlantaSave(){
        $bot_data = $this->global_robot();
        $bot_model = $this->model("plant"); // carrego o model na variavel
        $name_plant = $this->input("popName"); //aqui voce pega o valor do post, o text dentro da função é o mesmo que voce usuaria em $_POST['name_plant']
        $order_plant = $this->input("order");
        $bot_insert['name'] = $name_plant; // aqui voce cria um vetor onde a chave é o nome da coluna no banco e o valor é o que voce quer inserir
        $bot_insert['order'] = $order_plant; // aqui voce cria um vetor onde a chave é o nome da coluna no banco e o valor é o que voce quer inserir
        $bot_model->insertPlant($bot_insert); // aqui voce chama pela sua função do model, inserindo os dados que voce passou
        echo "passei";
    }
    public function testAddon(){
        $bot_agro = $this->addon(1);
        $bot_agro->hard("GPIO")->pin(1)->type("O")->Volt("H")->set();
        $bot_agro->hard("GPIO")->pin(2)->type("I")->Volt("H")->set();
        //$bot_agro->hard("GPIO")->pin(1)->type("O")->Volt("L")->set();
        $bot_agro->send();
    }
}

?>