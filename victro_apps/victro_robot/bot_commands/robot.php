<?php

class bot_commands extends controller_robot {
    public function hear(){
        $phrase = $this->input("PHRASE", "POST_GET");
        if($phrase != false){
            if($this->functions($phrase) == true){
                echo $this->conversation("yes");
            } else {
                echo $this->conversation($phrase);
            }
        } else {
            $this->system_view();
        }
    }
    public function phrase($phrase) {
        $bot_robotvars = $this->global_robot();
        $this->allow_user_type_by('1');

        $username = '6df27d1d-3179-4dac-9e4c-6d41e5a003cd';
        $password = 'cvwgJDxYxm0P';
        $bot_phrase_key = null;
        if (strlen($phrase) > 0) {
            $bot_phrase_key = 0;
            $bot_model = $this->model('phrase');
            $bot_DBphrase = $bot_model->select_phrase($phrase);
            if ($bot_DBphrase->get_count() > 0) {
                $bot_row = $bot_DBphrase->get_row();
                $bot_phrase_key = $bot_row->id;
                //echo "EXISTE";
            } else {
                $data = json_encode(array('text' => $phrase));
                $URL = 'https://gateway.watsonplatform.net/tone-analyzer/api/v3/tone?version=2016-05-19';

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $URL);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);


                $result = curl_exec($ch);
                curl_close($ch);
                $newResult = json_decode($result);
                //echo "<pre>";
                if (count($newResult) > 0) {
                    $bot_emotion_tone = $newResult->document_tone->tone_categories[0];
                    $bot_language_tone = $newResult->document_tone->tone_categories[1];
                    $bot_social_tone = $newResult->document_tone->tone_categories[2];
                    //print_r($bot_emotion_tone);
                    $bot_insertPhrase['PHRASE'] = $phrase;
                    $bot_insertPhrase['EMOTION_ANGER'] = $bot_emotion_tone->tones[0]->score;
                    $bot_insertPhrase['EMOTION_DISGUST'] = $bot_emotion_tone->tones[1]->score;
                    $bot_insertPhrase['EMOTION_FEAR'] = $bot_emotion_tone->tones[2]->score;
                    $bot_insertPhrase['EMOTION_JOY'] = $bot_emotion_tone->tones[3]->score;
                    $bot_insertPhrase['EMOTION_SADNESS'] = $bot_emotion_tone->tones[4]->score;
                    $bot_insertPhrase['LANGUAGE_ANALYTICAL'] = $bot_language_tone->tones[0]->score;
                    $bot_insertPhrase['LANGUAGE_CONFIDENT'] = $bot_language_tone->tones[1]->score;
                    $bot_insertPhrase['LANGUAGE_TENTATIVE'] = $bot_language_tone->tones[2]->score;
                    $bot_insertPhrase['SOCIAL_OPENNESS'] = $bot_social_tone->tones[0]->score;
                    $bot_insertPhrase['SOCIAL_CONSCIENTIOUSNESS'] = $bot_social_tone->tones[1]->score;
                    $bot_insertPhrase['SOCIAL_EXTRAVERSION'] = $bot_social_tone->tones[2]->score;
                    $bot_insertPhrase['SOCIAL_AGREEABLENESS'] = $bot_social_tone->tones[3]->score;
                    $bot_insertPhrase['SOCIAL_EMOTIONAL_RANGE'] = $bot_social_tone->tones[4]->score;
                    $bot_model->insert_phrase($bot_insertPhrase);
                    $bot_phrase_key = $bot_model->get_last_id();
                    //print_r($bot_insertPhrase);
                }
            }
        }
        return($bot_phrase_key);
    }
    public function conversation($phrase){
        $bot_num_phrase = $this->phrase($phrase);
        //$data = array("query" => $phrase, "lang" => "pt-BR", "sessionId" => "1234567890");                                                                    
        $data = array("query" => $phrase, "lang" => "en", "sessionId" => "1234567890");                                                                    
        $data_string = json_encode($data);                                                                                   

        $ch = curl_init('https://api.api.ai/v1/query?v=20150910');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
        //    'Content-Length: ' . strlen($data_string),
            'Content-type: application/json',
            'Authorization: Bearer 62c26465e6234d309fbeeca2642cdc9c',
            'ocp-apim-subscription-key: XxxX-xxx')
        );
        
        /*curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
        //    'Content-Length: ' . strlen($data_string),
            'Content-type: application/json',
            'Authorization: Bearer 094bd4b60f8f4e1db0be8ebb452b7083',
            'ocp-apim-subscription-key: XxxX-xxx')
        ); */

        $result = curl_exec($ch); 
        $newResult = json_decode($result);
        //print_r($newResult);
        $resultPhrase = $newResult->result->fulfillment;
        if($resultPhrase->speech != ""){
            $bot_num_reply = $this->phrase($resultPhrase->speech);
            if(isset($resultPhrase->messages)){
                foreach($resultPhrase->messages as $similar){
                    $bot_num_similar = $this->phrase($similar->speech);
                }
            }
            echo $resultPhrase->speech;
        } else {
            $this->conversation("I want you to answer me");
            //$this->conversation("quero sua resposta");
        }
        //$this->view('voice', $data);
    }
    public function functions($phrase){
        $url = "";
        $returnCheck = false;
        if($phrase == "turn it off" || $phrase == "turn off" || $phrase == "off"){
            $url = "http://192.168.1.235/?pin=OFF1";
        } else if($phrase == "turn it on" || $phrase == "turn on" || $phrase == "on"){
            $url = "http://192.168.1.235/?pin=ON1";
        }
        if($url != ""){
            $returnCheck = true;
            // create a new cURL resource
            $ch = curl_init();

            // set URL and other appropriate options
            curl_setopt($ch, CURLOPT_URL, $url);
            //curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            // grab URL and pass it to the browser
            curl_exec($ch);

            // close cURL resource, and free up system resources
            curl_close($ch);
        }
        return $returnCheck;
    }
}

?>