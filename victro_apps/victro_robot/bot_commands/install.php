<?php

$victro_robot->name("BotCommands");
$victro_robot->try_route("robot");
$victro_robot->try_route("ro_bot");
$victro_robot->author("Jean Victor");
$victro_robot->description("Controls");
$victro_robot->version(1.0);
$victro_robot->icon("fa fa-heart-o");
$victro_robot->update("http://victrobot.com/update/[KEY]");

//TABLE INFORMATION
$victro_robot->table("PHRASE")->engine("INNODB")->if_table("NOT EXISTS");
$victro_robot->column("ID")->type("INT")->value("11")->autoincrement(true)->index("PRIMARY KEY");
$victro_robot->column("PHRASE")->type("varchar")->value("255");
$victro_robot->column("EMOTION_ANGER")->type('float')->value('5');
$victro_robot->column("EMOTION_DISGUST")->type('float')->value('5');
$victro_robot->column("EMOTION_FEAR")->type('float')->value('5');
$victro_robot->column("EMOTION_JOY")->type('float')->value('5');
$victro_robot->column("EMOTION_SADNESS")->type('float')->value('5');
$victro_robot->column("LANGUAGE_ANALYTICAL")->type('float')->value('5');
$victro_robot->column("LANGUAGE_CONFIDENT")->type('float')->value('5');
$victro_robot->column("LANGUAGE_TENTATIVE")->type('float')->value('5');
$victro_robot->column("SOCIAL_OPENNESS")->type('float')->value('5');
$victro_robot->column("SOCIAL_CONSCIENTIOUSNESS")->type('float')->value('5');
$victro_robot->column("SOCIAL_EXTRAVERSION")->type('float')->value('5');
$victro_robot->column("SOCIAL_AGREEABLENESS")->type('float')->value('5');
$victro_robot->column("SOCIAL_EMOTIONAL_RANGE")->type('float')->value('5');
$victro_robot->column("DELETED")->type('boolean')->value('');



$victro_robot->table("notas")->if_table("");
$victro_robot->column("id")->type("int")->value("11")->autoincrement(true)->index("PRIMARY KEY");
$victro_robot->column("nome")->type("varchar")->value("150");
$victro_robot->column("valor")->type('int')->value('11');
$victro_robot->column("fundo")->type('varchar')->value('7');
$victro_robot->column("letra")->type('varchar')->value('7');
$victro_robot->column("deletado")->type('int')->value('1');
//print_r($victro_robot->array_table);
//MENU INFORMATION
$victro_robot->menu("Modalidades", "3")->submenu('Gerenciar', '3')->submenu('Notas', '2');
$victro_robot->menu("Modalidades2", "5");
?>