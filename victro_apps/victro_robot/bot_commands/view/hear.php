<script src='https://code.responsivevoice.org/responsivevoice.js'></script>
<?php
if (!defined('PROTECT')) {
    exit('NO ACCESS');
}
/*
 * The MIT License
 *
 * Copyright 2017 contato.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
?>
<div id="said"></div>
<div id="voice"></div>
<script>
    startEar();
    var muteToEar = false;
    function startEar() {
        var recognition = new (window.SpeechRecognition || window.webkitSpeechRecognition || window.mozSpeechRecognition || window.msSpeechRecognition)();
        recognition.lang = 'en-GB';
        //recognition.lang = 'pt-BR';
        recognition.interimResults = false;
        recognition.maxAlternatives = 5;
        recognition.continuous = true;
        recognition.start();

        recognition.onresult = function (event) {
            console.log(event.results);
            var div = document.getElementById('said');
            var numSaid = 0;
            if (typeof event.results.length !== 'undefined') {
                numSaid = event.results.length - 1;
            }
            div.innerHTML = div.innerHTML + "<BR>" + event.results[numSaid][0].transcript;

            var setdata = "PHRASE=" + event.results[numSaid][0].transcript;
            $.ajax({
                type: "POST",
                global: false,
                url: 'hear',
                data: setdata,
                success: function (dados1) {
                    var parameters = {
                        onstart: function () {
                            muteToEar = true;
                            recognition.abort();
                        },
                        onend: function () {
                            muteToEar = false;
                            recognition.start();
                        }
                    }
                    responsiveVoice.speak(dados1, "UK English Male", parameters);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        };
        recognition.onend = function () {
            if (muteToEar == false) {
                recognition.start();
            }
        }
        
    }
    function voicetest(text) {
            var setdata = "PHRASE=" + text;
            $.ajax({
                type: "POST",
                global: false,
                url: 'hear',
                data: setdata,
                success: function (dados1) {
                    responsiveVoice.speak(dados1, "UK English Male");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }
</script>



