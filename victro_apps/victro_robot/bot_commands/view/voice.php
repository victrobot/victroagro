<html>
	<head>
		<title></title>
		<script src='https://code.responsivevoice.org/responsivevoice.js'></script>
	</head>
	<body>
		<div id="said"></div>
		<script> responsiveVoice.speak("<?php echo $say; ?>", "UK English Male"); 
		startEar();
		function startEar(){
			var recognition = new (window.SpeechRecognition || window.webkitSpeechRecognition || window.mozSpeechRecognition || window.msSpeechRecognition)();
			recognition.lang = 'en-GB';
			//recognition.lang = 'pt-BR';
			recognition.interimResults = false;
			recognition.maxAlternatives = 5; 
			recognition.continuous = true;
			recognition.start();

			recognition.onresult = function(event) {
				console.log(event.results);
				var div = document.getElementById('said');
				var numSaid = 0;
				if(typeof event.results.length !== 'undefined'){
					numSaid = event.results.length - 1;
				}
				div.innerHTML = div.innerHTML +"<BR>"+ event.results[numSaid][0].transcript;
			};
			recognition.onend = function() {
				startEar();
			}
		}
		</script>
	</body>
</html>