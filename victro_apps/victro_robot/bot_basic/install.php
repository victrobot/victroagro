<?php

$victro_robot->name("BotBasic");
$victro_robot->try_route("robot");
$victro_robot->try_route("ro_bot");
$victro_robot->author("Jean Victor");
$victro_robot->description("Basics");
$victro_robot->version(1.0);
$victro_robot->icon("fa fa-heart-o");
$victro_robot->update("http://victrobot.com/update/[KEY]");

//TABLE INFORMATION
$victro_robot->table("LANGUAGE")->engine("INNODB")->if_table("NOT EXISTS");
$victro_robot->column("ID")->type("INT")->value("11")->autoincrement(true)->index("PRIMARY KEY");
$victro_robot->column("LANGUAGE")->type("varchar")->value("60");
$victro_robot->column("SHORT_LANG")->type('varchar')->value('10');
$victro_robot->column("DELETED")->type('boolean')->value('1');

//MENU INFORMATION
$victro_robot->menu("BOT BASIC", "3")->submenu(array('language' => '3'));
?>