<?php
class BotBasic extends controller_robot {
    public function language($bot_pag = 1, $bot_mod = 1){
        $bot_data = $this->global_robot();
        $bot_model = $this->model("language");
        $bot_data['returned'] = $bot_model->select_language($bot_pag)->get_fetch();
        $bot_data['pags'] = $bot_model->bot_numpags;
        if($bot_mod == 1){
            $this->system_view("language", $bot_data);
        } else {
            $this->view("language", $bot_data);
        }
    }
    public function saveData(){
        $bot_model = $this->model("basic");
        $bot_insert = array();
        foreach($_POST as $bot_postkey => $bot_post){
            $bot_insert[$bot_postkey] =  $bot_post;
        }
        $bot_insert['deleted'] = 0;
        echo $bot_model->in_up_Data($bot_insert);
    }
    public function testAddon(){
        $bot_agro = $this->addon(1);
        $bot_agro->hard("GPIO")->pin(1)->type("O")->Volt("H")->read()->set();
        $bot_agro->hard("GPIO")->pin(2)->type("I")->Volt("H")->set();
        //$bot_agro->hard("GPIO")->pin(1)->type("O")->Volt("L")->set();
        $bot_agro->send();
    }
}

?>