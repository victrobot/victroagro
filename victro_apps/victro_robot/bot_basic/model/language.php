<?php

class language extends model_robot {
    public $bot_numpags = 0;
    public $bot_actual_pag = 0;
    public function select_language($bot_pag) {
        $bot_num_pp = 20;
        $this->bot_actual_pag = $bot_num_pp * ($bot_pag - 1);
        $this->select("*");
        $this->from('bot_LANGUAGE');
        $this->where("DELETED <> 1");
        $query1 = $this->db_select(false);
        $bot_numTot = $query1->get_count();
        $this->bot_numpags = ceil($bot_numTot/ $bot_num_pp);
        $this->limit($this->bot_actual_pag.",".$bot_num_pp);
        $query = $this->db_select();
        return($query);
    }
}

?>