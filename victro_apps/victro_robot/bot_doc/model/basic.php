<?php

class basic extends model_robot {
    private $bot_table = "bot_LANGUAGE";
    public function select_data() {
        $this->select("*");
        $this->from($this->bot_table);
        $this->where("DELETED <> 1");
        $query = $this->db_select();
        return($query);
    }

    public function insertData($values) {
        $query = $this->db_insert($this->bot_table, $values);
        return($query);
    }

    public function updateData($nId, $values) {
        $this->where('ID = ' . $nId);
        $query = $this->db_update($this->bot_table, $values);
    }
    
    public function in_up_Data($values){
        $bot_ret = 0;
        if(isset($values['ID']) and $values['ID'] > 0){
            $nId = $values['ID'];
            unset($values['ID']);
            $this->updateData($nId, $values);
        } else {
            unset($values['ID']);
            $bot_ret = $this->insertData($values);
        }
        return($bot_ret);
    }
}

?>