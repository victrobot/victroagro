<?php

$victro_robot->name("BotDoc");
$victro_robot->try_route("documentation");
$victro_robot->try_route("doc");
$victro_robot->author("Jean Victor");
$victro_robot->description("Software Documentation");
$victro_robot->version(1.0);
$victro_robot->icon("fa fa-book");
$victro_robot->update("http://victrobot.com/update/[KEY]");

//TABLE INFORMATION
$victro_robot->table("DOC_FUNCTION")->engine("INNODB")->if_table("NOT EXISTS");
$victro_robot->column("ID")->type("INT")->value("11")->autoincrement(true)->index("PRIMARY KEY");
$victro_robot->column("CLASS")->type("INT")->value("11");
$victro_robot->column("FUNCTION")->type('varchar')->value('150');
$victro_robot->column("PARAMS")->type('text')->value('');
$victro_robot->column("RETURNS")->type('text')->value('');
$victro_robot->column("AUTHOR")->type('varchar')->value('200');
$victro_robot->column("DEPRECATED")->type('boolean')->value('');
$victro_robot->column("DESCRIPTION")->type('text')->value('');
$victro_robot->column("EXEMPLE")->type('text')->value('');
$victro_robot->column("DELETED")->type('boolean')->value('');

$victro_robot->table("DOC_CLASS")->engine("INNODB")->if_table("NOT EXISTS");
$victro_robot->column("ID")->type("INT")->value("11")->autoincrement(true)->index("PRIMARY KEY");
$victro_robot->column("CLASS")->type("varchar")->value("100");
$victro_robot->column("DELETED")->type('boolean')->value('');

//MENU INFORMATION
$victro_robot->menu("Documentation", "1")->submenu('Index', '1')->submenu('Classes', '3')->submenu('Functions', '3');
?>