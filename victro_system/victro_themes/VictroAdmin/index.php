<?php if(!defined('PROTECT')){ exit('NO ACCESS'); } ?>
<?php include('model/header.php'); ?>
<?php include('model/topbar.php'); ?>
<?php include('model/sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
       <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">  
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php victro_translate("Settings"); ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <!-- content here -->
                    <div class="col-md-12">
                        <ul class="nav nav-tabs">
                                <li class="active"><a href="#basic" data-toggle="tab" aria-expanded="true"><?php victro_translate('basic'); ?></a></li>
                                <li class=""><a href="#advanced" data-toggle="tab" aria-expanded="false"><?php victro_translate('advanced'); ?></a></li>
                                <li class=""><a href="#pages" data-toggle="tab" aria-expanded="false"><?php victro_translate('pages'); ?></a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="basic">
                                <p>
                                    <form method="post">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sitename"><?php victro_translate('site name'); ?>:</label>
                                            <input type="text" name="sitename" value="<?php echo $victro_site_name; ?>" class="form-control">
                                        </div>
                                        <BR>
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <label class="control-label " for="urlsite"><?php victro_translate('Site url'); ?>:</label>
                                            <select name="urltype" class="form-control">
                                                    <option value="http://" <?php if($victro_site['http'] == 'http://'){ echo 'selected ';} ?> >HTTP</option>
                                                    <option value="https://" <?php if($victro_site['http'] == 'https://'){ echo 'selected ';} ?>>HTTPS</option>
                                            </select>
                                        </div><BR><BR><BR>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" name="siteurl" value="<?php echo $victro_site['url']; ?>" class="form-control" >
                                        </div>

                                        <br /><BR><BR>
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="freeuser"><?php victro_translate('free user'); ?>:</label>
                                            <select name="freeuser" class="form-control">
                                                    <option value="1" <?php if($victro_security == 1){ echo 'selected ';} ?>>ON</option>
                                                    <option value="0" <?php if($victro_security == 0){ echo 'selected ';} ?>>OFF</option>
                                            </select>
                                        </div>
                                        <br /><BR><BR><BR>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="submit" class="btn btn-success" name="sendset" value="<?php victro_translate('save'); ?>">
                                        </div>
                                    </form><Br><BR><BR>
                                </p>
                            </div>
                            <div class="tab-pane fade" id="advanced">
                                <p>
                                    <form method="post">									
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <select name="hometype" class="form-control" onchange="if(this.value == 'plugin'){ plugin_display.style.display = 'inline'; } else { plugin_display.style.display = 'none'; }  " onmouseover="if(this.value == 'plugin'){ plugin_display.style.display = 'inline'; } " >
                                                    <option value="index" <?php if($victro_index_input == 'index'){ echo 'selected'; } ?>>Index</option>
                                                    <option value="plugin" <?php if($victro_index_input != 'index'){ echo 'selected'; } ?> >Plugin</option>
                                            </select>
                                        </div>
                                        <div id="plugin_display" style="display:none">
                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                <select name="pluginslist" class="form-control">
                                                        <?php
                                                        $victro_i = 0;
                                                        foreach ($victro_plugins['name'] as $victro_namep) {
                                                                $victro_slct = null;
                                                                if($victro_plugin_active == $victro_plugins['id'][$victro_i]){
                                                                        $victro_slct = 'selected';
                                                                }
                                                                echo '<option value="'.$victro_plugins['id'][$victro_i].'" '.$victro_slct.' >'.$victro_namep.'</option>';
                                                                $victro_i++;
                                                        } ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                <input type="text" name="action" value="<?php echo $victro_acao ; ?>" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <input type="submit" class="btn btn-success" name="sendset" value="<?php victro_translate('save'); ?>">
                                        </div>

                                    </form><BR><BR><BR></p>
                                </div>
                                <div class="tab-pane fade" id="pages">
                                    <p>					
                                        <form method="post">
                                            <?php
                                            $victro_i = 0;
                                            echo '<input type="hidden" name="friendly" value="send">';
                                            foreach ($victro_plugins['name'] as $victro_namep) {
                                                    echo '<div class="col-md-6 col-sm-6 col-xs-12"><input class="form-control" readonly name="friendpluginname'.$victro_plugins['id'][$victro_i].'" value="'.$victro_namep.'" ></div>';
                                                    echo '<div class="col-md-6 col-sm-6 col-xs-12"><input class="form-control" type="text" name="urlplugin'.$victro_plugins['id'][$victro_i].'" value="'.$victro_plugins['value'][$victro_i].'"></div>';
                                                    echo "<BR><BR>";
                                                    $victro_i++;
                                            } ?><BR>
                                            <input type="submit" class="btn btn-success" name="sendset" value="<?php victro_translate('save'); ?>">
                                        </form>
                                    </p>
                                </div>
                        </div>
                    </div>
                </div>
                    <!-- content ends -->
                </div>
            </div>
        </div>
    </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include('model/footer.php'); ?>
